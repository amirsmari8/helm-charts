# install an helm chart with values file or set options (wordpress as an example): 
1) Searches the Helm Hub repository for charts related to WordPress: 
    * helm search hub wordpress
en format yaml :
    * helm search hub --output yaml wordpress
2) search for a repo related to wordpress as an example: 
    * helm search repo wordpress
3) add repo wordpress : 
    * helm repo add bitnami https://charts.bitnami.com/bitnami
4) update repo: 
    * helm update repo bitnami
    * helm repo list
5) search for local repo: 
    * helm search repo wordpress
    * helm search repo --versions wordpress
6) Displays the chart information (metadata) : 
    * helm show chart bitnami/wordpress
    * helm show readme bitnami/wordpress
    * helm show values bitnami/wordpress
    * helm show all bitnami/wordpress
7) Installs the Bitnami WordPress chart onto your Kubernetes cluster under the release name "mywp".
    * helm install mywp bitnami/wordpress
    * helm uninstall mywp
8) important command: 
    * helm list -a 

# Helm Structure & Create
charts = templates + values + metadatas 
create charts: $ helm create test 
# helm configmaps (to deploy multiple configmaps) 
> deploy the helm charts: 
$ helm install mycm configmaps/ 
$ helm list 
> when we do a change on our helm , we need to do an upgrade: 
$ helm upgrade mycm configmaps/ 
> if we want to rollback to an older revision 
$ helm rollback mycm 3  
> we need to check also the history: 
$ helm history mycm 
--- 
# Built-in Objects
Objects are passed into a template from the template engine.  
differents Objects: 
  * release

  * values

  * chart

  * files

  * capabilities

  * template
example: 
* .Release.Name

* .Release.Namespace

* .Release.IsUpgrade

* .Release.IsInstall

* .Release.Revision

* .Release.Service 

# Conditions: if ... default ... ternaire ... fail
Note: for number , always use quote. ex: 
  key: {{ default 1 | quote .Values.var1 }}  

# Functions  
1) trim, trimPrefix, trimSuffix
fuction used to remove specified characters from the beginning, end, or both ends of a string, respectively. 
* trim:  trim " Salut " would result in "Salut". 
* trimAll:  trimAll "+" "+10" would result in "10", removing all occurrences of +. 
* trimPrefix: trimPrefix "+" "+200" would result in "200", removing the prefix +.
* trimSuffix: trimSuffix "." "200." would result in "200", removing the suffix ..
2) quote "" and squote '' 
3) INDENT/NINDENT: indent 4 means add 4 spaces
4) REPLACE: ex "Hello-Xavki" | replace "-" " "


# With, Range and Scopes: 
see example 5/6 configmap charts
to define a variable "amir" in GO template, ex: 
{{ $amir := .Release.Name }}