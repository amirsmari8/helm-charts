## General 
You could use subPath like this to mount single file into existing directory:
volumeMounts:
        - name: "config"
          mountPath: "/<existing folder>/<file1>"
          subPath: "<file1>"

# Usefull Notes 
3 method to deploy helm charts: 
  * CLI (à éviter) 
  * values.yaml 
  * HelmRealease 
  


## usefull command: 
helm template 
helm upgrade 

## FQDN of service: 
the FQDN of any service by default would be <service-name>.<namespace>.svc.cluster.local


## helm upgrade 
* to upgrade , change the Version at Chart.yaml

if we do an helm upgrade(upgrade a release) , the revision will change from 1 to 2 as an example. 
$ helm upgrade redis      # redis: release name // revision will change from 1 to 2

## helm rollback
$ helm rollback redis 1     # redis: release name // 1: is the revision (return to revision 1)

## helm history of revision: 
$ helm history redis   # redis is release name


* helm wait and rollback (atomic): https://polarsquad.com/blog/check-your-helm-deployments
## helm wait 
--wait, Helm will wait until a minimum expected number of Pods in the deployment are launched before marking the release as successful. Helm will wait as long as what is set with --timeout. By default, the timeout is set to 300 seconds. Let’s try it out! 
example: 
$ helm upgrade --wait --timeout 20 demo demo/

## helm atomic 
* Automated rollbacks with atomic
Helm install and upgrade commands include an --atomic CLI option, which will cause the chart deployment to automatically rollback when it fails. Enabling the atomic option will automatically enable wait.
example
$ helm upgrade --atomic --timeout 20 --set readinessPath=/ demo demo/

## helm dependencies: 
example: we need to deplay a backend (api) but it requires a database to be present. 
so we can specify in the helm-charts that the database need to be deplyed as well. 
* the are 3 ways to define dependencies in the helm-charts: 
1 - dependency conditions 
2 - enabled flag 
3 - tags 
>>>> dependency conditions
*1.1 ) in the parents charts (under Chart.yaml), we add as an example: 
dependencies:
- name: helm_redis       # see it in the Chart.yaml of redis
  version: "0.1.0"       # see it in the Chart.yaml of redis
  condition: database.enabled       # need to be specified in Values.yam of helm_redis      
  repository: "file://../helm_redis"
*1.2 ) we will tape the command: 
$helm dependency update helm_api/    : update based on file Charts.yaml of api_helm 
this command will download redis charts under foler "charts" as archive charts.
*1.3 ) then we can install charts api and we will have 2 deployment redis and api. 

>>>>>>>>> enabled flag 
*2.1 ) in the parents charts (under Chart.yaml), we add as an example: 
dependencies:
- name: helm_redis       # see it in the Chart.yaml of redis
  version: "0.1.0"       # see it in the Chart.yaml of redis
  enabled: true       #no modification in values.yaml of redis charts     
  repository: "file://../helm_redis"



## Create Helm Private Chart Repository (helm package & helm push chart)
- helm plugin install https://github.com/hypnoglow/helm-s3.git
- helm plugin list
- helm s3 version --mode 
- helm-educap.io 
- helm s3 init s3://helm-educap.io/charts # it will create a folder charts and index.yml inside the charts folder
- helm repo add amir-repo s3://helm-educap.io/charts
- helm repo list 
- helm package helm_api/     # we will have helm_api-0-1-0.tgz
- helm s3 push --relative ./helm_api-0-1-0.tgz amir-repo
- helm search amir-repo
- helm install api amir-repo/helm_api 


## Helm 3 Secret Management (Helm 3 Secrets Plugin)
1- create key pair and extract the fingerprint.
we are going to use key pair (public and private key), the public key is going to encrypt the secret. helm will decrypt the secret using the private key.
But to encrypt the secret, we are going to use "the fingreprint" of the key: 
$ gpg --gen-key                     ## to generate the public and private key 
$ gpg --list-keys

2- create the secret using: Mozilla SOPS (install it manually)
$ sops -p (fingerprint of the key) secrets.yaml 
Now le'ts test to decrypt the secret using "helm secret plugin": 
$helm secrets decrypt secrets.yaml 

$helm secrets install api ./helm_api -f ./secrets.yaml



This plugin provides ability to encrypt/decrypt secrets files:
- helm plugin install https://github.com/jkroepke/helm-secrets 
- helm secrets help


## Mozilla SOPS
https://medium.com/@minuddinahmedrana/secrets-management-with-sops-3ed3b2ceadaa





## RedisInsight
RedisInsight is a desktop manager that provides an intuitive and efficient GUI for Redis, allowing you to interact with your databases, monitor, and manage your data.
Redis is an in-memory databases (so when server is down all data will be lost, for that we need a persistent volume) 

## persistent volume 
2 solution : 
* RDB (Redis Database): The RDB persistence performs point-in-time snapshots of your dataset at        specified intervals. 
the snapshot: dump.rdb  
For example, this configuration will make Redis automatically dump the dataset to disk every 60 seconds if at least 1000 keys changed:
save 60 1000

* AOF (Append Only File): The AOF persistence logs every write operation received by the server, that will be played again at server startup, reconstructing the original dataset.
You can turn on the AOF in your configuration file:
appendonly yes
From now on, every time Redis receives a command that changes the dataset (e.g. SET) it will append it to the AOF. When you restart Redis it will re-play the AOF to rebuild the state.
How durable is the append only file?
You can configure how many times Redis will fsync data on disk. There are three options: 
** appendfsync always: fsync 
** appendfsync everysec: fsync 
** appendfsync no: Never fsync 

## persist configuration: 
dir /data
dbfilename dump.rdb
appendonly yes
appendfilename "appendonly.aof" 

## replication configuartion 
# redis-0 Configuration
protected-mode no
port 6379

#authentication
masterauth a-very-complex-password-here
requirepass a-very-complex-password-here

# redis-1 Configuration
protected-mode no
port 6379
slaveof redis-0 6379

#authentication
masterauth a-very-complex-password-here
requirepass a-very-complex-password-here

# redis-2 Configuration
protected-mode no
port 6379
slaveof redis-0 6379

#authentication
masterauth a-very-complex-password-here
requirepass a-very-complex-password-here

## what if the redis master is fail ? 
replication enable dosen't means we have HA , it means only that we replicate the data among multiple redis instance . 
to achienve high availability (if master is down , we switch to the slave) > this feature is called redis sentinel

## redis sentinel
Redis Sentinel provides high availability for Redis when not using Redis Cluster.

Sentinel capabilities : Monitoring, Notification, Automatic failover, Configuration provider.