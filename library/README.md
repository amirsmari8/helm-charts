## Repo principales: 
https://artifacthub.io/

## info: 
Release: une instanciation de la charts dans k8s 
to search: helm search repo wordpress

usefull link: https://www.youtube.com/watch?v=gbUBTTXuQwI&list=PLLYW3zEOaqlKYku0piyzzLFGpR9VpPvXR

docker run -it --name golang -p 90:8080 -e DB_HOST=172.17.0.2:3306 -e DB_PASS=root -e DB_NAME=library golang

docker run -ti --name mysql -p 3306:3306 -v /scripts/sql.sql:/docker-entrypoint-initdb.d/sql.sql -e MYSQL_ROOT_PASSWORD=root mysql


docker run -ti --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root mysql:spec

## for test
mysql -h 172.17.0.2 -uroot -proot -P 3306  

// DB_HOST is of form host:port

### STDOUT and STDERR
STDOUT (Standard Output), which contains information logs and success messages, and STDERR (Standard Error), which contains error messages.

1 indicate STDOUT 
2 indicate STDERR 
so we can use : $command 2>/dev/null 
                $command 1>/dev/null 

to discard both STDOUT and STDERR at the same time, use the following: $command &>/dev/null 

&:  denotes that both the standard output and standard error has to be redirected to /dev/null and discarded                


## Secret 
echo -n 'root' | base64
echo -n 'root' | base64 --decode


## livennes probes 
we can do also :
livenessProbe:
exec:
command:
- bash
- "-c"
- |
mysqladmin -uroot -p$MYSQL_ROOT_PASSWORD ping &> /dev/null 





## StatefullSet with pvc and pv 
https://zhimin-wen.medium.com/persistent-volume-claim-for-statefulset-8050e396cc51
https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/ 

vscode extentions needed: helm , helm intellisence

to create a helm charts: 
$helm create helm-charts #helm-charts created as a folder 

to render a template: 
$helm template test helm-charts     #test: Release.Name, and helm-charts is the folder 


{{- }} : no new line and no empty space
  

## volume with helm charts 
https://exchangetuts.com/how-best-to-have-files-on-volumes-in-kubernetes-using-helm-charts-1640531046216755 

## usefull command: 
helm template 
helm upgrade 

## FQDN of service: 
the FQDN of any service by default would be <service-name>.<namespace>.svc.cluster.local


## helm upgrade 
* to upgrade , change the Version at Chart.yaml

if we do an helm upgrade(upgrade a release) , the revision will change from 1 to 2 as an example. 
$ helm upgrade redis      # redis: release name // revision will change from 1 to 2

## helm rollback
$ helm rollback redis 1     # redis: release name // 1: is the revision (return to revision 1)

## helm history of revision: 
$ helm history redis   #redis is release name


* helm wait and rollback (atomic): https://polarsquad.com/blog/check-your-helm-deployments
## helm wait 
--wait, Helm will wait until a minimum expected number of Pods in the deployment are launched before marking the release as successful. Helm will wait as long as what is set with --timeout. By default, the timeout is set to 300 seconds. Let’s try it out! 
example: 
$ helm upgrade --wait --timeout 20 demo demo/

## helm atomic 
* Automated rollbacks with atomic
Helm install and upgrade commands include an --atomic CLI option, which will cause the chart deployment to automatically rollback when it fails. Enabling the atomic option will automatically enable wait.
example
$ helm upgrade --atomic --timeout 20 --set readinessPath=/ demo demo/
